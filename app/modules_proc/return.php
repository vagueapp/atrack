<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['assetAssign','returnDate'])){ throw new aterror('CHS','Assignment Return Submission Missing Required Values',100); }

    $assetDB->returnAsset(
                            $sub['assetAssign'],
                            $sub['returnDate']);
?>
