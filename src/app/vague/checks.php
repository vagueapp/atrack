<?php
    namespace app\vague
    {
        class checks
        {
            public static function arraySet(array $checkArray, array $values){
                foreach($values as $val){
                    if(!isset($checkArray[$val])){
                        return FALSE;
                    }
                }
                return TRUE;
            }

            public static function constraints(array $checkArray){
            /*
                Array   [
                            $key0 => $val0  [
                                                'function' => <required_value>,
                                            ],
                        ]
            */
                if(count($checkArray) == 0){
                    return FALSE;
                }
                foreach($checkArray as $key0=>$val0){
                    if(is_array($val0)){
                        foreach($val0 as $key1=>$val1){
                            if(call_user_func($key1,$key0) !== $val1)
                            {
                                return FALSE;
                            }
                        }
                    }
                }
                return TRUE;
            }
        }
    }
?>
