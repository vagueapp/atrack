<?php
    namespace app\vague\atrack\db
    {
        class getters
        {
            protected $dbc=[
                                  'driver'=>null,
                                  'host'=>null,
                                  'port'=>3306,
                                  'user'=>null,
                                  'pass'=>null,
                                  'database'=>null,
                                 ];
            protected $pdo=null;

            public function __construct($inifile)
            {
                //FunctionID:0
                if(file_exists($inifile)){
                    $inicontents = parse_ini_file($inifile,FALSE);
                    $this->dbc['driver'] = $inicontents['driver'];
                    $this->dbc['host'] = $inicontents['host'];
                    $this->dbc['user'] = $inicontents['username'];
                    $this->dbc['pass'] = $inicontents['password'];
                    $this->dbc['database'] = $inicontents['database'];
                }else{
                    throw new aterror('INI','Unable to Read And/Or Parse INI File',0);
                }

                try {
                    $this->pdo = new \PDO(
                                    $this->dbc['driver'].
                                    ":host=".$this->dbc['host'].
                                    ";port=".$this->dbc['port'].
                                    ";dbname=".$this->dbc['database'],
                                    $this->dbc['user'],
                                    $this->dbc['pass'],
                                    [
                                        \PDO::MYSQL_ATTR_FOUND_ROWS => TRUE, //To ensure updating an asset (without changes) doesn't error
                                    ]);
                } catch (\PDOException $e){
                    throw new aterror('CON','Could Not Connect To Database');
                    return;
                }
            }

            public function getEmployees()
            {
                //FunctionID:1
                $query =    "SELECT
                                id,
                                CONCAT(firstName,' ',lastName) AS name
                            FROM
                                employees
                            ORDER BY
                                name";
                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get Employee List From DB',1);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get Employee List From DB',1);
                }
                $emp = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $emp[$row['id']] = $row['name'];
                }
                return $emp;
            }

            public function getAssets()
            {
                //FunctionID:2
                $query =    "SELECT
                                id,
                                serialNumber,
                                CONCAT(make,' ',model) AS info,
                                make,
                                model,
                                assetTag
                            FROM
                                assets
                            ORDER BY
                                info,
                                serialNumber";
                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get Asset List From DB',2);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get Asset List From DB',2);
                }
                $ast = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $ast[$row['id']]['serialNumber'] = $row['serialNumber'];
                    $ast[$row['id']]['info'] = $row['info'];
                    $ast[$row['id']]['make'] = $row['make'];
                    $ast[$row['id']]['model'] = $row['model'];
                    $ast[$row['id']]['assetTag'] = $row['assetTag'];
                }
                return $ast;
            }

            public function getAsset($id)
            {
                //FunctionID:3
                $query =    "SELECT
                                *
                            FROM
                                assets
                            WHERE
                                id = :id";
                try {
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':id',$id,\PDO::PARAM_INT);
                    if(!$qres->execute()){
                        throw new aterror('DBQE','Failure To Get Asset From DB',3);
                    }
                }catch (\Exception $e){
                    throw new aterror('DBQ','Failure To Get Asset From DB',3);
                }
                if($qres->rowCount() == 0){
                    return false;
                }
                return $qres->fetch(\PDO::FETCH_ASSOC);
            }
            
            public function getMakeList()
            {
                //FunctionID:4
                $query =    "SELECT DISTINCT
                                make
                            FROM
                                assets
                            ORDER BY
                                make";
                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get List From DB',4);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get List From DB',4);
                }
                $ml = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    array_push($ml,$row['make']);
                }
                return $ml;
            }

            public function getModelList()
            {
                //FunctionID:5
                $query =    "SELECT DISTINCT
                                model
                            FROM assets
                            ORDER BY
                                model";
                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get List From DB',5);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get List From DB',5);
                }
                $ml = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    array_push($ml,$row['model']);
                }
                return $ml;
            }

            public function getAssetTypes()
            {
                //FunctionID:6
                $query =    "SELECT
                                id,
                                value AS assetType
                            FROM
                                lists
                            WHERE
                                type = 'assetType'
                            ORDER BY
                                assetType";

                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get List From DB',6);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get List From DB',6);
                }
                $at = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $at[$row['id']] = $row['assetType'];
                }
                return $at;
            }

            public function getAssetAssignments(array $search=[])
            {
                //FunctionID:7
                $query =    "SELECT
                                aa.id,
                                a.serialNumber,
                                a.assetTag,
                                a.make,
                                a.model,
                                aa.assignmentDate,
                                aa.returned,
                                CONCAT(e.firstName,' ',e.lastName) AS name
                            FROM
                                assetAssignment AS aa
                            INNER JOIN employees AS e
                                ON aa.employeeID = e.id
                            INNER JOIN assets AS a
                                ON aa.assetID = a.id";

                $queryEnd = " ORDER BY aa.assignmentDate ASC";
                try {
                    switch(true){
                        case(empty($search['employee']) && !empty($search['asset'])):
                            $query = $query." WHERE aa.assetID = :asset";
                            $qres = $this->pdo->prepare($query.$queryEnd);
                            $qres->bindValue(':asset',$search['asset'],\PDO::PARAM_INT);
                            break;
                        case(empty($search['asset']) && !empty($search['employee'])):
                            $query = $query." WHERE aa.employeeID = :employee";
                            $qres = $this->pdo->prepare($query.$queryEnd);
                            $qres->bindValue(':employee',$search['employee'],\PDO::PARAM_INT);
                            break;
                        case(!empty($search['asset']) && !empty($search['employee'])):
                            $query = $query." WHERE aa.assetID = :asset AND aa.employeeID = :employee";
                            $qres = $this->pdo->prepare($query.$queryEnd);
                            $qres->bindValue(':asset',$search['asset'],\PDO::PARAM_INT);
                            $qres->bindValue(':employee',$search['employee'],\PDO::PARAM_INT);
                            break;
                        default:
                            $qres = $this->pdo->prepare($query.$queryEnd);
                            break;
                    }
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get Asset Assignments From DB',7);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Get Asset Assignments From DB',7);
                }
                $al = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $al[$row['id']] = $row;
                }
                return $al;
            }

            public function getEmployeesWOID()
            {
                //FunctionID:8
                $query =    "SELECT
                                id,
                                CONCAT(firstName,' ',lastName) as name,
                                empID
                            FROM
                                employees
                            WHERE
                                empID IS NULL";

                try {
                    $qres = $this->pdo->prepare($query);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get Employee List From DB',8);
                    }
                }catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Get Employee List From DB',8);
                }
                if($qres->rowCount() == 0){
                    return false;
                }
                $el = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $el[$row['id']] = $row['name'];
                }
                return $el;
            }

            public function getEmployeeWOID($id)
            {
                //FunctionID:9
                $query =    "SELECT
                                id,
                                CONCAT(firstName,' ',lastName) as name,
                                empID
                            FROM
                                employees
                            WHERE
                                empID IS NULL
                                AND
                                id = :id";
                try {
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':id',$id,\PDO::PARAM_INT);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get Employee From DB',9);
                    }
                }catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Get Employee From DB',9);
                }
                if($qres->rowCount() == 0){
                    return false;
                }
                $result = $qres->fetch(\PDO::FETCH_ASSOC);
                return ['id' => $result['id'], 'name' => $result['name']];
            }

            public function getList(string $listType)
            {
                //FunctionID:10
                $query =    "SELECT
                                id,
                                value
                            FROM
                                lists
                            WHERE
                                type = :type";
                $qres = $this->pdo->prepare($query);
                try {
                    $qres->bindValue(':type',$listType,\PDO::PARAM_STR);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Get List Of Type: '.$listType.' From DB',10);
                    }
                } catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Get List Of Type: '.$listType.' From DB',10);
                }
                $results = [];
                while($row = $qres->fetch(\PDO::FETCH_ASSOC)){
                    $results[$row['id']] = $row['value'];
                }
                return $results;
            }
        }

    }
?>
