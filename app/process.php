<?php
    header("Content-type: text/plain");

    use \app\vague\atrack\db\aterror;

    $assetDB = new \app\vague\atrack\db\setters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);

    $sub = $_POST;

    if(empty($sub) || !isset($sub['data']['action'])){
        echo "Invalid Test Use of File";
        echo "Please Ensure Calling Files Is Sending Correct POSTDATA";
        exit;
    }

    switch($sub['data']['action']){
        case ('assign'):
            require_once(_ATMODULESDIRPROC_ . '/assign.php');
            break;
        case ('addasset'):
            require_once(_ATMODULESDIRPROC_ . '/addasset.php');
            break;
        case ('return'):
            require_once(_ATMODULESDIRPROC_ . '/return.php');
            break;
        case ('addemployee'):
            require_once(_ATMODULESDIRPROC_ . '/addemployee.php');
            break;
        case ('updateid'):
            require_once(_ATMODULESDIRPROC_ . '/updateid.php');
            break;
        case ('updateasset'):
            require_once(_ATMODULESDIRPROC_ . '/updateasset.php');
            break;
        default:
            throw new aterror('PROC','Malformed Information Sent To Processor',100);
            break;
    }
    header('Location: ./'. _ATENTRY_ . '?p=result&s=Success&r='.$sub['data']['return'].'&a='.$sub['data']['module']); 
?>
