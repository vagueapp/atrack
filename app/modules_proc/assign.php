<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['employee','asset','date','location','assignmentType'])){ throw new aterror('CHS','Asset Assignment Submission Missing Required Values',100); }
    if(!checks::constraints([
                            $sub['employee']    => ['\is_numeric' => TRUE],
                            $sub['asset']       => ['\is_numeric' => TRUE],
                        ])){ throw new aterror('CHC','Assignment Submission Contains Invalid Values',100); }

    $assetDB->insertAssignment(
                                $sub['employee'],
                                $sub['asset'],
                                $sub['date'],
                                $sub['location'] ?? null,
                                $sub['assignmentType']);
?>
