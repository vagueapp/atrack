<?php
    namespace app\vague
    {
        class format
        {
            public static function idt(int $n=1,string $i='    ')
            {
                return(self::indent($n,$i));
            }
            
            public static function indent(int $n=1, $indentChar='    ')
            {
                return(str_repeat($indentChar,$n));
            }

            public static function loopprint(array $userArray, string $userString, array $options = [])
            {
                $usingCustom = false;
                if(isset($options['customSR']) && is_array($options['customSR'])){
                    $usingCustom = true;
                    $search = [];
                    foreach($options['customSR'] as $key=>$val){
                        $search[$key] = $val;
                    }
                }
                
                $search = [':k:',':v:',':n:'];
                $output = '';
                $counter = 0;
                $indent = (isset($options['indent']) && is_int($options['indent']) && $options['indent'] > 0 ) ? $options['indent'] : 0;
                $first = (isset($options['skipFirstIndent']) && $options['skipFirstIndent'] == true) ? true : false;

                foreach($userArray as $key=>$val){
                    if($usingCustom){
                        $ts = $userString;
                        foreach($search as $skey=>$sval){
                            $ts = str_replace($skey,$val[$sval],$ts);
                        }
                    }else{
                        $ts = str_replace($search,[$key,$val,"\n"],$userString);
                    }
                    if($indent > 0 && !$first){
                        $ts = self::indent($indent).$ts;
                    }
                    if(isset($options['select']) && $options['select'] == $key){
                        $ts = str_replace(':s:',' selected',$ts);
                    }else{
                        $ts = str_replace(':s:','',$ts);
                    }
                    $output = $output . $ts;
                    $first = false;
                }
                return $output;
            }
        }
    }
?>
