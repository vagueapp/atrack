<?php

    use \app\vague\format;
    //$assetDB = new \obi\db\getters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);

    $title = 'Add A New Employee';
    $data = [
                "module" => "addemployee",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "addemployee",
            ];

    $formAction = _ATENTRY_ . '?p=process';

    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>
                            <script>
                                document.addEventListener('DOMContentLoaded',function(){
                                    document.getElementById('firstName').addEventListener('paste',niceFieldPaste);
                                });
                                
                                function niceFieldPaste(e)
                                {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    var dbForm = document.querySelector('table[class=dbform]').querySelectorAll('input[type=text]');
                                    var pasted = e.clipboardData.getData('text/plain')
                                    var pastedSplit = pasted.split(';');
                                    if (pastedSplit.length === 5)
                                    {
                                       var step = 0;
                                       for (step = 0; step < 5; step++)
                                       {
                                            dbForm[step].value = pastedSplit[step];
                                       }
                                    }else{
                                        dbForm[0].value = pasted;
                                    }
                                }
                            </script>
                            <table class="dbform" style="border: none;">
                                <tr>
                                    <td>
                                        <label for="firstName">First Name:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="firstName" id="firstName" required />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="lastName">Last Name:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="lastName" id="lastName" required />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="email">Email:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="email" id="email" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="empID">Employee ID:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="empID" id="empID" placeholder="Include if known" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="startDate">Start Date:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="startDate" id="startDate" pattern="[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}" value="<?=date('Y-m-');?>" required />
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
                            </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
