<?php
    $title='Results';
    $status = $_GET['s'] ?? 'Results';
    $redirect = $_GET['r'] ?? 'index.php';
    $action = $_GET['a'] ?? 'menu';
    $redirectURI = $redirect.'?p='.$action;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta http-equiv="refresh" content="5;url=./<?=_ATENTRY_;?>" />
        <title><?=$title;?></title>
        <link rel="stylesheet" href="./main.css" />
        <link rel='shortcut icon' href='./favicon.png' />
    </head>
    <body>
        <div id="wrap">
            <div class="row full-height">
                <div id="main-left">
                    &nbsp;
                </div>
                <div id="main-right">
                    <div class="container">
                        <h3>
                            <?=$status;?>
                        </h3>
                        You will be redirected to <a href="./index.php">the main menu</a> in 5 seconds. 
                        <br />
                        Please click the link if you're not redirected shortly.
                        <br />
                        Or you can <a href="./<?=$redirectURI;?>">go back the page you came from</a>.
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
