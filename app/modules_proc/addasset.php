<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['serialNumber','make','model','status','assetType','riskLevel','criticality','environment'])){ throw new aterror('CHS','Asset Addition Submission Missing Required Values',100); }
    if(!checks::constraints([
                            $sub['assetType']   =>['\is_numeric'=>TRUE],
                        ])){ throw new aterror('CHC','Asset Addition Submission Contains Invalid Values',100); }

    $assetDB->insertAsset(
                            $sub['serialNumber'],
                            $sub['assetTag'] ?? null,
                            $sub['assetName'] ?? null,
                            $sub['assetType'],
                            $sub['make'],
                            $sub['model'],
                            $sub['serviceCode'] ?? null,
                            $sub['status'],
                            $sub['riskLevel'],
                            $sub['riskOwner'] ?? null,
                            $sub['criticality'],
                            $sub['environment'],
                            $sub['macLAN'] ?? null,
                            $sub['macWLAN'] ?? null,
                            $sub['IMEI'] ?? null,
                            $sub['invoiceNum'] ?? null,
                            $sub['vendor'] ?? null,
                            $sub['purchaseDate'] ?? null,
                            $sub['warantyPeriod'] ?? null);
?>
