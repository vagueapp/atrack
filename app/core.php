<?php
    
    define('_ATENTRY_',basename($_SERVER['PHP_SELF']));
    //define('_ATENTRYDIR_',dirname($_SERVER['SCRIPT_FILENAME']));
    define('_ATENTRYDIR_',getcwd());
    define('_ATAPPDIR_',__DIR__);
    define('_ATBASEDIR_',_ATAPPDIR_ . '/..');
    define('_ATMODULESDIRACT_', _ATAPPDIR_ . '/modules_act');
    define('_ATMODULESDIRPROC_', _ATAPPDIR_ .'/modules_proc');
    define('_ATINCLUDESDIR_', _ATAPPDIR_ . '/includes');
    
    if(!defined('_ATCONFIGDIR_')){
        define('_ATCONFIGDIR_',_ATBASEDIR_ . '/config');
    }

    require_once(_ATBASEDIR_ . '/vendor/autoload.php');

    use \app\vague\atrack\db\aterror;
    
    try {
        require_once(_ATINCLUDESDIR_ . '/global.php');

        /* Force Show The Error Page */
        if(isset($_GET['break']) && $_GET['break'] == "debug"){
            throw new aterror('DBG','Manually Triggered Debug Error',999);
            exit(1);
        }

        $page = $_GET['p'] ?? '';

        switch($page){
            case('process'):
                require_once(_ATBASEDIR_.'/app/process.php');
                break;
            case('result'):
                require_once(_ATBASEDIR_.'/app/result.php');
                break;
            case(''):
                if(!loadModule('menu',$config,$atrackCfg)){ throw new aterror('MOD','Default Module Missing or Inaccessible',100); }
                break;
            default:
                if(!loadModule($page,$config,$atrackCfg)){ throw new aterror('MOD','Specified Module Missing or Inaccessible',100); }
                break;
        }
    } catch (aterror $e) {
        echo $e->getCustomMessage();
        exit(1);
    }

?>
