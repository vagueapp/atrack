<?php

    use \app\vague\format;
    use \app\vague\atrack\db\aterror;

    $title = 'Mark an Asset as Returned';

    $assets = $assetDB->getAssets();
    foreach($assets as $key=>$val){ $assets_f[$key] = $val['info'].' '.$val['serialNumber']; }

    $users = $assetDB->getEmployees();
    $phase = 0;
    $atrackCfg['method'] = 'get';
    $atrackCfg['skipHidden'] = TRUE;
    $data = [
                "module" => "return",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "return",
            ];

    if(!empty($_GET['asset']) || !empty($_GET['employee'])){
        $phase = 1;
        $atrackCfg['method'] = 'post';
        $atrackCfg['skipHidden'] = FALSE;
        $search['asset'] = (!empty($_GET['asset'])) ? $_GET['asset'] : null;
        $search['employee'] = (!empty($_GET['employee'])) ? $_GET['employee'] : null;
        $assetAssigns = $assetDB->getAssetAssignments($search);
    }

    /*
    if($serialSecond){
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['info'].' - '.$val['serialNumber']; }
    }else{
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['serialNumber'].' - '.$val['info']; }
        asort($assets_f);
    }
    */
    $assets_f = sortAssets($assets,$assetSortField);

    $formAction = _ATENTRY_ . '?p=' . (($phase==1) ? 'process' : $data['module']);

    $showSortSwitch = TRUE;
    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>

                            <table class="dbform" style="border: none; border-collapse: collapse;">
<?php if($phase == 0){ ?>
                                <tr>
                                    <td colspan="2">Please select an employee, assset, or both to list active assignments.</td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <label for="employee">Employee:</label>
                                    </td>
                                    <td>
                                        <select id="employee" name="employee">
                                            <option value="">--Employees--</option>
                                            <?=format::loopprint($users,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>true]);?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        --and/or--
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="asset">Asset:</label>
                                    </td>
                                    <td>
                                        <select id="asset" name="asset">
                                            <option value="">--Assets--</option>
                                            <?=format::loopprint($assets_f,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>true]);?>
                                       </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="p" value="<?=$data['module'];?>" />
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
    }else{
?>
                                <tr class="border">
                                    <th>Name</th>
                                    <th>Make/Model</th>
                                    <th>Serial</th>
                                    <th>Asset Tag</th>
                                    <th>Assignment Date</th>
                                    <th>Return Date</th>
                                    <th>Select</th>
                                </tr>
<?php
        foreach($assetAssigns as $key=>$val){
?>
                                <tr class="border<?php if(!is_null($val['returned'])){ echo " returned"; }?>">
                                    <td><?=$val['name'];?></td>
                                    <td><?=$val['make']." ".$val['model'];?></td>
                                    <td><?=$val['serialNumber'];?></td>
                                    <td><?=$val['assetTag'];?></td>
                                    <td><?=$val['assignmentDate'];?></td>
                                    <td><?=$val['returned'];?></td>
                                    <td style="text-align: center;"><?php if(empty($val['returned'])) { ?><input type="radio" name="assetAssign" value="<?=$val['id'];?>" required="required" /><?php } ?></td>
                                </tr>
<?php
        }
        if(count($assetAssigns) > 0){
?>
                                <tr class="border">
                                    <td>
                                        <label for="returnDate">Date of Return: </label>
                                    </td>
                                    <td colspan="6">
                                        <input type="text" name="returnDate" id="returnDate" pattern="[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}" value="<?=date('Y-m-');?>" />
                                    </td>
                                </tr>
                                <tr><td colspan="7">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="7">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
        }
    }
?>
                            </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
