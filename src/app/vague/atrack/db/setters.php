<?php
    namespace app\vague\atrack\db
    {
        class setters extends getters
        {
            public function insertAssignment($employee,$asset,$date,$location,$type)
            {
                //FunctionID:0
                $query =     "INSERT INTO
                                assetAssignment
                                    (employeeID,assetID,assignmentDate,location,assignmentType)
                            VALUES
                                (:eid,:aid,:adate,:loc,:atype)";
                try{
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':eid',$employee,\PDO::PARAM_INT);
                    $qres->bindValue(':aid',$asset,\PDO::PARAM_INT);
                    $qres->bindValue(':adate',$date,\PDO::PARAM_STR);
                    if(is_null($location)){
                        $qres->bindValue(':loc',null,\PDO::PARAM_INT);
                    }else{
                        $qres->bindValue(':loc',$location,\PDO::PARAM_INT);
                    }
                    $qres->bindValue(':atype',$type,\PDO::PARAM_STR);
                    if (! $qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',0);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Insert New Assignment',0);
                }
                return true;
            }

            public function insertAsset($serial,$tag,$name,$type,$make,$model,$servicecode,$status,$risklevel,$riskowner,$critic,$environment,$maclan,$macwlan,$imei,$invoicenum,$vendor,$purchase,$waranty)
            {
                //FunctionID:1
                $query =    "INSERT INTO
                                assets
                                    (serialNumber,assetTag,assetName,assetType,make,model,serviceCode,status,riskLevel,riskOwner,criticality,environment,macLAN,macWLAN,IMEI,invoiceNum,vendor,purchaseDate,warantyPeriod)
                            VALUES
                                (:serial,:tag,:name,:type,:make,:model,:service,:status,:riskl,:risko,:critic,:environment,:mlan,:mwlan,:imei,:invoice,:vendor,:purchase,:waranty)";
                try{
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':serial',$serial,\PDO::PARAM_STR);
                    $qres->bindValue(':tag',$tag,\PDO::PARAM_STR);
                    $qres->bindValue(':name',$name,\PDO::PARAM_STR);
                    $qres->bindValue(':type',$type,\PDO::PARAM_INT);
                    $qres->bindValue(':make',$make,\PDO::PARAM_STR);
                    $qres->bindValue(':model',$model,\PDO::PARAM_STR);
                    $qres->bindValue(':service',$servicecode,\PDO::PARAM_STR);
                    $qres->bindValue(':status',$status,\PDO::PARAM_INT);
                    $qres->bindValue(':riskl',$risklevel,\PDO::PARAM_INT);
                    $qres->bindValue(':risko',$riskowner,\PDO::PARAM_INT);
                    $qres->bindValue(':critic',$critic,\PDO::PARAM_INT);
                    $qres->bindValue(':environment',$environment,\PDO::PARAM_INT);
                    $qres->bindValue(':mlan',$maclan,\PDO::PARAM_STR);
                    $qres->bindValue(':mwlan',$macwlan,\PDO::PARAM_STR);
                    $qres->bindValue(':imei',$imei,\PDO::PARAM_STR);
                    $qres->bindValue(':invoice',$invoicenum,\PDO::PARAM_STR);
                    $qres->bindValue(':vendor',$vendor,\PDO::PARAM_STR);
                    $qres->bindValue(':purchase',$purchase,\PDO::PARAM_STR);
                    $qres->bindValue(':waranty',$waranty,\PDO::PARAM_STR);
                    if (! $qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',1);
                    }
                }catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Insert New Assignment',1);
                }
                return true;
            }

            public function insertEmployee($firstName,$lastName,$email,$empID,$startDate)
            {
                //FunctionID:2
                $query =    "INSERT INTO
                                employees (firstName,lastName,empID,email,startDate)
                            VALUES
                                (:firstName,:lastName,:empID,:email,:start)";
                try {
                    $qres=$this->pdo->prepare($query);
                    $qres->bindValue(':firstName',$firstName,\PDO::PARAM_STR);
                    $qres->bindValue(':lastName',$lastName,\PDO::PARAM_STR);
                    $qres->bindValue(':empID',$empID,\PDO::PARAM_STR);
                    $qres->bindValue(':email',$email,\PDO::PARAM_STR);
                    $qres->bindValue(':start',$startDate,\PDO::PARAM_STR);
                    if (! $qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',2);
                    }
                } catch (\PDOException $e) {
                    throw new aterror('DBQ','Failure To Insert New Assignment',2);
                }
                return true;
            }

            public function updateID($id,$empID)
            {
                //FunctionID:3
                $query =    "UPDATE
                                employees
                            SET
                                empID = :empID
                            WHERE
                                empID IS NULL
                                AND
                                id = :id";
                try {
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':id',$id,\PDO::PARAM_INT);
                    $qres->bindValue(':empID',$empID,\PDO::PARAM_STR);
                    if(! $qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',3);
                    }
                } catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Insert New Assignment',3);
                }
                if($qres->rowCount() == 0){
                    return false;
                }
                return true;
            }

            public function updateAsset($id,$assetTag,$assetName,$status,$riskLevel,$riskOwner,$criticality,$environment)
            {
                //FunctionID:4
                $query =    "UPDATE
                                assets
                            SET
                                assetTag = :assetTag,
                                assetName = :assetName,
                                status = :status,
                                riskLevel = :riskLevel,
                                riskOwner = :riskOwner,
                                criticality = :criticality,
                                environment = :environment
                            WHERE
                                id = :id";
                try {
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':id',$id,\PDO::PARAM_INT);
                    $qres->bindValue(':assetTag',$assetTag,\PDO::PARAM_STR);
                    $qres->bindValue(':assetName',$assetName,\PDO::PARAM_STR);
                    $qres->bindValue(':status',$status,\PDO::PARAM_INT);
                    $qres->bindValue(':riskLevel',$riskLevel,\PDO::PARAM_INT);
                    $qres->bindValue(':riskOwner',$riskOwner,\PDO::PARAM_INT);
                    $qres->bindValue(':criticality',$criticality,\PDO::PARAM_INT);
                    $qres->bindValue(':environment',$environment,\PDO::PARAM_INT);
                    if(!$qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',4);
                    }
                } catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Insert New Assignment',4);
                }
                if($qres->rowCount() == 0){
                    return false;
                }
                return true;
            }

            public function returnAsset($asset,$date)
            {
                //FunctionID:5
                $query =    "UPDATE
                                assetAssignment
                            SET
                                returned = :date
                            WHERE id = :asset";
                try{
                    $qres = $this->pdo->prepare($query);
                    $qres->bindValue(':asset',$asset,\PDO::PARAM_INT);
                    $qres->bindValue(':date',$date,\PDO::PARAM_STR);
                    if(!$qres->execute()){
                        throw new aterror('DBQE','Failure To Insert New Assignment',5);
                    }
                } catch (\PDOException $e){
                    throw new aterror('DBQ','Failure To Insert New Assignment',5);
                }
                return true;
            }
        }
    }
?>
