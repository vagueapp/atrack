<?php

    use \app\vague\format;

    $title = 'Update An Asset';

    $assets = $assetDB->getAssets();
    $assets_f = [];
    foreach($assets as $key=>$val){ $assets_f[$key] = $assets[$key]['info'].' '.$assets[$key]['serialNumber']; }

    $phase = 0;
    $data = [
                "module" => "updateasset",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "updateasset",
            ];

    if(!empty($_POST['asset'])){
        $phase = 1;
        $lists['statuses'] = $assetDB->getList('status');
        $lists['environments'] = $assetDB->getList('environment');
        $lists['riskLevels'] = $assetDB->getList('riskLevel');
        $lists['criticalities'] = $assetDB->getList('criticality');
        $users = $assetDB->getEmployees();
        $asset = $assetDB->getAsset($_POST['asset']);
        if($asset === false){
            throw new \app\vague\atrack\db\aterror('RES','Non-Existant Assest Chosen',100);
            exit(1);
        }
    }

    /*
    if($serialSecond){
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['info'].' - '.$val['serialNumber']; }
    }else{
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['serialNumber'].' - '.$val['info']; }
        asort($assets_f);
    }
    */
    $assets_f = sortAssets($assets,$assetSortField);

    $formAction = _ATENTRY_ . '?p=' . (($phase==1) ? 'process' : $data['module']);

    $showSortSwitch = TRUE;
    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>
                            <table class="dbform" style="border: none; border-collapse: collapse;">
<?php if($phase == 0){ ?>
                                <tr>
                                    <td colspan="2">
                                        Please select an asset to update.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="asset">Asset:</label>
                                    </td>
                                    <td>
                                        <select id="asset" name="asset" required>
                                            <option value="">--Assets--</option>
                                            <?= format::loopprint($assets_f,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]);?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
    }else{
?>
                                <tr>
                                    <td>
                                        <label for="serialNumber">Serial Number</label>
                                    </td>
                                    <td>
                                        <input type="hidden" name="id" value="<?=$asset['id'];?>" />
                                        <span id="serialNumber"><?=$asset['serialNumber'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="make">Make</label>
                                    </td>
                                    <td>
                                        <span id="make"><?=$asset['make'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="model">Model</label>
                                    </td>
                                    <td>
                                        <span id="model"><?=$asset['model'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="assetTag">Asset Tag</label>
                                    </td>
                                    <td>
                                        <input type="text" name="assetTag" id="assetTag" value="<?=$asset['assetTag'] ?? '';?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="assetName">Asset Name</label>
                                    </td>
                                    <td>
                                        <input type="text" name="assetName" id="assetName" value="<?=$asset['assetName'] ?? '';?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="status">Status</label>
                                    </td>
                                    <td>
                                        <select name="status" id="status" required>
                                            <option value="">--Select a Status--</option>
                                            <?= format::loopprint($lists['statuses'],'<option value=":k:":s:>:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE,'select'=>$asset['status']]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="riskLevel">Risk Level</label>
                                    </td>
                                    <td>
                                        <select name="riskLevel" id="riskLevel" required>
                                            <option value="">--Select a Risk Level--</option>
                                            <?= format::loopprint($lists['riskLevels'],'<option value=":k:":s:>:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE,'select'=>$asset['riskLevel']]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="riskOwner">Risk Owner</label>
                                    </td>
                                    <td>
                                        <select name="riskOwner" id="riskOwner" required>
                                            <option value="">--Select a Risk Owner--</option>
                                            <?= format::loopprint($users,'<option value=":k:":s:>:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE,'select'=>$asset['riskOwner']]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="criticality">Criticality</label>
                                    </td>
                                    <td>
                                        <select name="criticality" id="criticality" required>
                                            <option value="">--Select a Risk Level--</option>
                                            <?= format::loopprint($lists['criticalities'],'<option value=":k:":s:>:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE,'select'=>$asset['criticality']]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="environment">Environment</label>
                                    </td>
                                    <td>
                                        <select name="environment" id="environment" required>
                                            <option value="">--Select an Environment--</option>
                                            <?= format::loopprint($lists['environments'],'<option value=":k:":s:>:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE,'select'=>$asset['environment']]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr >
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
    }
?>
                            </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
