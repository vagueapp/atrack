<?php

    use \app\vague\format;
    //$assetDB = new \obi\db\getters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);

    $title = 'Add A New Asset';

    $makes = $assetDB->getMakeList();
    $models = $assetDB->getModelList();
    $assetTypes = $assetDB->getAssetTypes();
    $employees = $assetDB->getEmployees();
    $environments = $assetDB->getList('environment');
    $statuses = $assetDB->getList('status');
    $riskLevels = $assetDB->getList('riskLevel');
    $criticalities = $assetDB->getList('criticality');
    $data = [
                "module" => "addasset",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "addasset",
            ];
    $formAction = _ATENTRY_ . '?p=process';

    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>
                            <table class="dbform" style="border: none;">
                                <tr>
                                    <td>
                                        <label for="serialNumber">Serial Number:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="serialNumber" id="serialNumber" required />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="assetTag">Asset Tag:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="assetTag" id="assetTag" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="assetName">Asset Name:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="assetName" id="assetName" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="assetType">Asset Type:</label>
                                    </td>
                                    <td>
                                       <select name="assetType" id="assetType" required>
                                            <option value="">--Select an Asset Type--</option>
                                            <?= format::loopprint($assetTypes,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label for="make">Make:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="make" id="make" list="makel" required />
                                        <datalist id="makel">
                                            <?= format::loopprint($makes,'<option value=":v:" />:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </datalist>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="model">Model:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="model" id="model" list="modell" required />
                                        <datalist id="modell">
                                            <?= format::loopprint($models,'<option value=":v:" />:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </datalist>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="serviceCode">Service Code:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="serviceCode" id="serviceCode" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="status">Status</label>
                                    </td>
                                    <td>
                                        <select name="status" id="status" required>
                                            <option value="">--Select a Status--</option>
                                            <?= format::loopprint($statuses,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="riskLevel">Risk Level:</label>
                                    </td>
                                    <td>
                                        <select name="riskLevel" id="riskLevel" required>
                                            <option value="">--Select a Risk Level--</option>
                                            <?= format::loopprint($riskLevels,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="riskOwner">Risk Owner:</label>
                                    </td>
                                    <td>
                                        <select name="riskOwner" id="riskOwner">
                                            <option value="">-Not Specified-</option>
                                                <?= format::loopprint($employees,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="criticality">Criticality:</label>
                                    </td>
                                    <td>
                                        <select name="criticality" id="criticality" required>
                                            <option value="">--Select a Criticality Level--</option>
                                            <?= format::loopprint($criticalities,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="environment">Environment:</label>
                                    </td>
                                    <td>
                                        <select name="environment" id="environment" required>
                                            <option value="">--Select an Environment--</option>
                                            <?= format::loopprint($environments,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]); ?>
                                        </select>
                                <tr>
                                    <td>
                                        <label for="macLAN">MAC Address (LAN):</label>
                                    </td>
                                    <td>
                                        <input type="text" name="macLAN" id="macLAN" pattern="([a-fA-F0-9]{2}[-: ]?){6}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="macWLAN">MAC Address (WLAN):</label>
                                    </td>
                                    <td>
                                        <input type="text" name="macWLAN" id="macWLAN" pattern="([a-fA-F0-9]{2}[-: ]?){6}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="IMEI">IMEI:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="IMEI" id="IMEI" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="vendor">invoiceNum:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="invoiceNum" id="invoiceNum" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="vendor">Vendor:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="vendor" id="vendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="purchaseDate">Purchase Date:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="purchaseDate" id="purchaseDate" pattern="[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="warantyPeriod">Waranty Period:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="warantyPeriod" id="warantyPeriod" pattern="[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}" />
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
                            </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
