<?php

    use \app\vague\format;
    $files = listModules();

    if(isset($pageAct)){
        $formAction = $formAction.$pageAct;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?= $config['title'].' - '.$title; ?></title>
        <link rel="stylesheet" href="./main.css" />
        <link rel='shortcut icon' href='./favicon.png' />
    </head>
    <body>
        <div id="wrap" class="full-height">
            <div class="row full-height">
                <div id="main-left">
                    <div id="nav">
                        <span class="heading">
                           &bull;&nbsp;<a href="./index.php">Main Menu</a>&nbsp;&bull;
                        </span>
                        <hr class="secbg" />
                        <ul>
<?php
    if(isset($config['Link List'])){
        foreach($config['Link List'] as $caption => $url){
            echo format::idt(7).'<li><a href="'.$url.'">'.$caption."</a></li>\n";
        }
    }    
    foreach($files as $key=>$val){
        if(isset($data['module']) && $data['module'].'.php' == $key){
            $arrow = ' class="list-arrow-in"';
        }else{
            $arrow = '';
        }
        echo format::idt(7).'<li'.$arrow.'><a href="./'._ATENTRY_.'?p='.str_replace('.php','',$key).'">'.$val.'</a></li>'."\n";
    }
?>
                        </ul>
                    </div>
                </div>
                <div id="main-right">
                    <div class="container">
                        <h2>
<?php
    echo format::idt(7).$title;
    if(isset($showSortSwitch) && $showSortSwitch && (!isset($phase) || $phase == 0)){
        echo "\n".format::idt(7)."<br />";
        echo "\n".format::idt(7).'<span style="font-size: 0.5em">'.$assetSortSwitch."</span>\n";
    }
?>
                        </h2>
                        <form action="./<?=$formAction;?>" method="<?=$atrackCfg['method'];?>">
                            <?php if(!$atrackCfg['skipHidden']){ echo format::loopprint($data,'<input type="hidden" name="data[:k:]" value=":v:" />:n:',['indent'=>7,'skipFirstIndent'=>TRUE]); } ?>
