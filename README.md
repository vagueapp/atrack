# ATrack

## Setup Instructions  
1. Place all of these files in a location your webserver/php can reach it
2. Run composer.php dump-autoload to generate the appropriate autoload files<sup>*</sup>
3. <TODO: Complete Setup Instructions>

<sup>*</sup>Note: If you do not have access to composer, a pre-generated 'vendor' directory is in the 'dist' directory.
