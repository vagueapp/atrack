<?php

    use \app\vague\format;
    //$assetDB = new \obi\db\getters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);

    $title = 'Assign an Asset to a User';

    $users = $assetDB->getEmployees();
    $assets = $assetDB->getAssets();
    $locations = $assetDB->getList('location');

    
    $data = [
                "module" => "assign",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "assign",
            ];

    /*
    if($serialSecond){
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['info'].' - '.$val['serialNumber']; }
    }else{
        foreach($assets as $key=>$val){ $assets_f[$key] = $val['serialNumber'].' - '.$val['info']; }
    }
    */
    $assets_f = sortAssets($assets,$assetSortField);

    $formAction = _ATENTRY_ . '?p=process';

    $showSortSwitch = TRUE;
    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>
                        <br />
                        <table class="dbform" style="border: none;">
                            <tr>
                                <td>
                                    <label for="employee">Employee:</label>
                                </td>
                                <td>
                                    <select id="employee" name="employee" required>
                                        <option value="">--Select a User--</option>
                                        <?=format::loopprint($users,'<option value=":k:">:v:</option>:n:',['indent'=>10,'skipFirstIndent'=>TRUE]);?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="asset">Asset:</label>
                                </td>
                                <td>
                                    <select id="asset" name="asset" required>
                                        <option value="">--Select an Asset--</option>
                                        <?=format::loopprint($assets_f,'<option value=":k:">:v:</option>:n:',['indent'=>10,'skipFirstIndent'=>TRUE]);?>
                                   </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="date">Date Assigned:</label>
                                </td>
                                <td>
                                    <input type="text" id="date" name="date" pattern="20[0-9]{2}-[0-9]{1,2}-[0-9]{1,2}" value="<?=date('Y-m-');?>" required />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="location">Location:</label>
                                </td>
                                <td>
                                    <select name="location" id="location">
                                        <option value="">--Select A Location--</option>
                                        <?=format::loopprint($locations,'<option value=":k:">:v:</option>:n:',['indent'=>10,'skipFirstIndent'=>TRUE]);?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="assignmentType">Assignment Type:</label>
                                </td>
                                <td>
                                    <select name="assignmentType" id="assignmentType">
                                        <option value="Dedicated" selected="selected" >Dedicated</option>
                                        <option value="Temporary">Temporary</option>
                                        <option value="Shared">Shared</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td colspan="2">
                                    <button type="submit" id="submit">Submit</button>
                                </td>
                            </tr>
                        </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
