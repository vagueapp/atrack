<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['id','assetTag','assetName','status','riskLevel','riskOwner','criticality','environment'])){ throw new aterror('CHS','Update Asset Submission Missing Required Values',100); }
    if(!checks::constraints([
                            $sub['id']          =>['\is_numeric'=>TRUE],
                            $sub['status']      =>['\is_numeric'=>TRUE],
                            $sub['riskLevel']   =>['\is_numeric'=>TRUE],
                            $sub['riskOwner']   =>['\is_numeric'=>TRUE],
                            $sub['criticality'] =>['\is_numeric'=>TRUE],
                            $sub['environment'] =>['\is_numeric'=>TRUE],
                        ])){ throw new aterror('CHC','Update Asset Submission Contains Invalid Values',100); }

    $assetDB->updateAsset(
                            $sub['id'],
                            $sub['assetTag'],
                            $sub['assetName'],
                            $sub['status'],
                            $sub['riskLevel'],
                            $sub['riskOwner'],
                            $sub['criticality'],
                            $sub['environment']);
?>
