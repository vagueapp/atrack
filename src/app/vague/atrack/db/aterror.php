<?php
    namespace app\vague\atrack\db
    {
        class aterror extends \Exception
        {
            protected $category = null;
            protected $message = null;
            protected $combined = null;
            protected $code = null;

            public function __construct($category = null, $message = null, $code = 0, \Exception $prev = null)
            {
                if(!isset($message)){
                    $this->message = $category;
                    $this->category = '[ATDB-GEN]';
                    $this->combined = $category.' '.$message;
                }else{
                    $this->category = '[ATDB-'.$category.']';
                    $this->message = $message;
                    $this->combined = $this->category.' '.$this->message;
                }
                $this->code = $code;
                parent::__construct($this->message,$code,$prev);
            }
        
            public function getCustomMessage()
            {
                if(defined('_ATINCLUDESDIR_') && file_exists(_ATINCLUDESDIR_ . '/error.html')){
                    $template = file_get_contents(_ATINCLUDESDIR_ . '/error.html');
                    $output = str_replace(
                                            [
                                                '[%CATEGORY%]',
                                                '[%MESSAGE%]',
                                                '[%CODE%]',
                                            ],
                                            [
                                                $this->category,
                                                $this->message,
                                                $this->code,
                                            ],
                                          $template);
                    echo $output;
                }else{
                    echo parent::getMessage();
                }
                exit(1);
            }

        }
    }
?>
