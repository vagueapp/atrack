<?php

    use \app\vague\format;
    //$assetDB = new \obi\db\getters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);

    $title = 'Add Missing Employee ID';

    $users = $assetDB->getEmployeesWOID();
    $phase = 0;
    $data = [
                "module" => "updateid",
                "return" => basename($_SERVER['PHP_SELF']),
                "action" => "updateid",
            ];

    if(!empty($_POST['employee'])){
        $phase = 1;
        $employee = $assetDB->getEmployeeWOID($_POST['employee']);
        if($employee === false){
            throw new \app\vague\atrack\db\aterror('RES','Invalid or Non-Existant Employee Selected',100);
        }
    }

    $formAction = _ATENTRY_ . '?p=' . (($phase==1) ? 'process' : $data['module']);

    require_once(_ATINCLUDESDIR_ . '/display.header.php');
?>
                            <table class="dbform" style="border: none; border-collapse: collapse;">
<?php if($phase == 0){ ?>
                                <tr>
                                    <td colspan="2">
                                        Please select an eligible employee.
                                            <br />
                                        <span style="font-size: 0.8em;">
                                            (Note: Only employees without an ID set are eligible.)
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="employee">Employee:</label>
                                    </td>
                                    <td>
                                        <select id="employee" name="employee" required>
                                            <option value="">--Employees--</option>
                                            <?=format::loopprint($users,'<option value=":k:">:v:</option>:n:',['indent'=>11,'skipFirstIndent'=>TRUE]);?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
    }else{
?>
                                <tr>
                                    <td>
                                        <label for="empName">Set ID For:</label>
                                    </td>
                                    <td>
                                        <input type="hidden" name="id" value="<?=$employee['id'];?>" />
                                        <span id="empName" style="font-weight: bold;">
                                            <?=$employee['name'];?>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="empID">Employee ID:</label>
                                    </td>
                                    <td>
                                        <input type="text" name="empID" id="empID" placeholder="Employee ID" required />
                                    </td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr >
                                    <td colspan="2">
                                        <button type="submit" id="submit">Submit</button>
                                    </td>
                                </tr>
<?php
    }
?>
                            </table>
<?php
    require_once(_ATINCLUDESDIR_ . '/display.footer.php');
?>
