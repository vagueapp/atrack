<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['firstName','lastName','startDate'])){ throw new aterror('CHS','Employee Addition Submission Missing Required Values',100); }

    $assetDB->insertEmployee(
                                $sub['firstName'],
                                $sub['lastName'],
                                $sub['email'] ?? null,
                                $sub['empID'] ?? null,
                                $sub['startDate']);
?>
