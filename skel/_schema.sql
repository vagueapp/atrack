/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assetAssignment`
--

DROP TABLE IF EXISTS `assetAssignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetAssignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeID` int(11) NOT NULL,
  `assetID` int(11) NOT NULL,
  `assignmentDate` date NOT NULL,
  `assignmentType` varchar(50) NOT NULL DEFAULT 'Dedicated',
  `location` int(11) DEFAULT NULL,
  `returned` date DEFAULT NULL,
  `dateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`employeeID`,`assetID`,`assignmentDate`),
  KEY `assignment_assetID` (`assetID`),
  KEY `id` (`id`),
  CONSTRAINT `assignment_assetID` FOREIGN KEY (`assetID`) REFERENCES `assets` (`id`),
  CONSTRAINT `assignment_employeeID` FOREIGN KEY (`employeeID`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serialNumber` varchar(100) NOT NULL,
  `assetTag` varchar(50) DEFAULT NULL,
  `assetType` int(11) NOT NULL DEFAULT '1',
  `assetName` varchar(50) DEFAULT NULL,
  `make` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `productID` varchar(100) DEFAULT NULL,
  `serviceCode` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `projectID` int(11) DEFAULT NULL,
  `riskLevel` int(11) NOT NULL,
  `riskOwner` int(11) DEFAULT NULL,
  `criticality` int(11) NOT NULL,
  `environment` int(11) DEFAULT NULL,
  `macLan` varchar(20) DEFAULT NULL,
  `macWlan` varchar(20) DEFAULT NULL,
  `IMEI` varchar(25) DEFAULT NULL,
  `invoiceNum` varchar(100) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `purchaseDate` date DEFAULT NULL,
  `warantyPeriod` varchar(255) DEFAULT NULL,
  `dateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `temp` int(11) DEFAULT NULL,
  `temp2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `serialNumber` (`serialNumber`),
  KEY `assets_assetType` (`assetType`),
  CONSTRAINT `assets_assetType` FOREIGN KEY (`assetType`) REFERENCES `lists` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empID` varchar(16) DEFAULT NULL,
  `firstName` tinytext,
  `lastName` tinytext,
  `email` tinytext,
  `startDate` date DEFAULT NULL,
  `termDate` date DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `dateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lists`
--

DROP TABLE IF EXISTS `lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT 'unknown',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reportingInfo`
--

DROP TABLE IF EXISTS `reportingInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportingInfo` (
  `employeeID` int(11) NOT NULL,
  `managerID` int(11) NOT NULL,
  `dateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`employeeID`,`managerID`),
  KEY `reporting_managerID` (`managerID`),
  CONSTRAINT `reporting_employeeID` FOREIGN KEY (`employeeID`) REFERENCES `employees` (`id`),
  CONSTRAINT `reporting_managerID` FOREIGN KEY (`managerID`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'atrack'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
