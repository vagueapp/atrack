<?php
    use \app\vague\checks;
    use \app\vague\atrack\db\aterror;

    if(!checks::arraySet($sub,['id','empID'])){ throw new aterror('CHS','Update Employee Submission Missing Required Values',100); }

    $assetDB->updateID(
                        $sub['id'],
                        $sub['empID']);
?>
