<?php

    use \app\vague\atrack\db\aterror;

    function loadModule($p,$config,$atrackCfg){
        if(file_exists(_ATMODULESDIRACT_.'/'.$p.'.php')){
            $assetDB = new \app\vague\atrack\db\getters(_ATCONFIGDIR_.'/db/'.$config['dbconf']);
            

            $quicklink = function($text,$val=null) use ($p) {
                if(is_null($val)){
                    return '<a href="'._ATENTRY_.'?p='.$p.'>'.$text.'</a>';
                }else{
                    return '<a href="'._ATENTRY_.'?p='.$p.'&asf='.$val.'">'.$text.'</a>';
                }
            };

            /* asf = asset sort field */
            $assetSortField = (isset($config['assetSort']) && in_array($config['assetSort'],['a','s','m'])) ? $config['assetSort'] : 'm';
            $assetSortSwitch = 'Sort By: '.$quicklink('Make/Model','m').' | '.$quicklink('Serial','s').' | '.$quicklink('Asset Tag','a');
            if(isset($_GET['asf'])) {
                switch($_GET['asf']){
                    case('s'):
                        $assetSortField = 's';
                        break;
                    case('a'):
                        $assetSortField = 'a';
                        break;
                    case('m'):
                        $assetSortField = 'm';
                        break;
                }
            }

//            $serialSecond = isset($_GET['ss']) ? TRUE : FALSE;
//            if($serialSecond){
//                $ssSwitch = '<a href="'._ATENTRY_.'?p='.$p.'">(Show Serial First)</a>';
//            }else{
//                $ssSwitch = '<a href="'._ATENTRY_.'?p='.$p.'&amp;ss=1">(Show Make/Model First)</a>';
//            }
            require_once(_ATMODULESDIRACT_.'/'.$p.'.php');
            return true;
        }else{
            return false;
        }
    }

    function filterme($a){
        $matcharray = [
                        '/^\..?+/',
                        '/.+\.swp/',
                        '/index.php$/',
                        '/.+\.css$/',
                      ];

        //Remove Directories From Listing
        if(is_dir($a)){ return false; }

        //Remove Regex Matches From Listing
        foreach($matcharray as $val){
            if(preg_match($val,$a)){
                return false;
            }
        }

        //Doesn't match the blacklisting above
        return true;
    }

    function listModules(){
        $modules = [];
        $iniArray = [];
        $modules=scandir(_ATMODULESDIRACT_);
        $modules=array_flip(array_filter($modules,'filterme'));
        if(is_file(_ATAPPDIR_ . '/listing.ini')){
            $iniArray = parse_ini_file(_ATAPPDIR_ . '/listing.ini',TRUE);
            foreach($modules as $key=>$val){
                if(array_key_exists($key,$iniArray['Blacklist'])){
                    unset($modules[$key]);
                }else{
                    if(array_key_exists($key,$iniArray['Files'])){
                        $modules[$key] = $iniArray['Files'][$key];
                    }else{
                        $modules[$key] = $key;
                    }
                }
            }
        }
        return $modules;
    }

    function sortAssets($assets,$assetSortField){
        $assets_f = [];
        $nomm = '&lt;No Make/Model&gt;';
        $noat = '&lt;No Asset Tag&gt;';

        switch($assetSortField){
            case('m'):
                foreach($assets as $key=>$val){
                    $assets_f[$key] = 
                        ((is_null($val['info']) || $val['info'] == '') ? $nomm : $val['info'])
                        .' - '
                        .$val['serialNumber']
                        .' - '
                        .((is_null($val['assetTag']) || $val['assetTag'] == '') ? $noat : $val['assetTag']);
                }
                break;
            case('s'):
                foreach($assets as $key=>$val){
                    $assets_f[$key] = 
                        $val['serialNumber']
                        .' - '
                        .((is_null($val['info']) || $val['info'] == '') ? $nomm : $val['info'])
                        .' - '
                        .((is_null($val['assetTag']) || $val['assetTag'] == '') ? $noat : $val['assetTag']);
                }
                break;
            case('a'):
                foreach($assets as $key=>$val){
                    $assets_f[$key] = 
                        ((is_null($val['assetTag']) || $val['assetTag'] == '') ? $noat : $val['assetTag'])
                        .' - '
                        .((is_null($val['info']) || $val['info'] == '') ? $nomm : $val['info'])
                        .' - '
                        .$val['serialNumber'];
                }
                break;
        }
        asort($assets_f);
        return $assets_f;
    }


    /*
        Begin Non-function Code
    */

    /* Set Some defaults */
    $atrackCfg = [
                    'method' => 'post',
                    'skipHidden' => FALSE,
                 ];

    if(file_exists(_ATCONFIGDIR_.'/config.ini')){
        //$config = parse_ini_file(_ATCONFIGDIR_.'/config.ini',FALSE);
        $config = parse_ini_file(_ATCONFIGDIR_.'/config.ini',TRUE,\INI_SCANNER_RAW);
        switch(true){
            case(empty($config['dbconf'])):
                throw new aterror('CFG','Database Config Value "dbconf" In Config File Is Missing or Empty',100);
                break;
        }
        /* Set Default Title If Not Set In INI */
        $config['title'] = isset($config['title']) ? $config['title'] : 'ATrack';

    }else{
        throw new aterror('CFG','Config File, "config.ini" Missing or Inaccessible.',100);
    }
?>
